from contextlib import contextmanager
from django.test.testcases import SimpleTestCase
import random
from tables.parse_utils import render_entry, nested_table_expression, table_expression

@contextmanager
def patch_parse_action(expression, replacement):
    """
    Patches the parse action for an expression, to avoid hitting the database.
    TODO: We should probably patch something else, to ensure that the nested table parse action works correctly.
    """
    original_action = expression.parseAction
    expression.setParseAction(replacement)
    yield
    expression.parseAction = original_action


class TestRenderEntry(SimpleTestCase):
    """
    Non-db tests of entry parsing.
    """
    def setUp(self):
        """
        Fix the dice roller before every single test.
        """
        random.seed(12)

    def run_subtests(self, cases):
        for test_string, expected_result in cases:
            with self.subTest(test_string=test_string, expected_result=expected_result):
                result = render_entry(test_string)
                self.assertEqual(result, expected_result)

    def test_simple_dice(self):
        cases = (
                 ('{1d1}', '1'),
                 ('{2d1}', '2'),
                 ('{2d1+2}', '4'),
                 ('{1d1*10}', '10'),
                 ('{1d100}', '48')
                 )
        self.run_subtests(cases)

    def test_dice_with_text(self):
        cases = (
            ('{2d1} rats', '2 rats'),
            ('You see {3d1} rats', 'You see 3 rats'),
            ('You see {2d1} rats and {4d1} orcs', 'You see 2 rats and 4 orcs'),
            ('{1d8+2} damage', '3 damage')
        )
        self.run_subtests(cases)

    def test_table_name(self):
        def dummy_table_action(s, l, token):
            return "roll on '%s' table" % token[0]

        with patch_parse_action(table_expression, dummy_table_action):
            res = render_entry('[Gems]')
            self.assertEqual(res, "roll on 'Gems' table")

    def test_table_name_with_dice(self):
        def dummy_table_action(s, l, token):
            """
            TODO: This does too much work; this logic should be in the table_expression's parseAction and
            we should only be patching the method that loads a table from the db.
            """
            if len(token) > 1:
                res = "{dice} roll(s) on '{table}' table".format(dice=token[0],
                                                                 table=token[1])
            else:
                res = "roll on '{table}' table".format(table=token[0])
            return res

        cases = (
                 ('[{3d6} Gems]', "13 roll(s) on 'Gems' table"),
                 ('{3d6} [Gems]', "14 roll on 'Gems' table"), # note dice outside table expression
                 ('[Gems]', "roll on 'Gems' table")
                 )
        with patch_parse_action(table_expression, dummy_table_action):
            self.run_subtests(cases)



