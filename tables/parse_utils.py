"""
Tools for parsing a simple DSL for random or relational entries.

Examples of TableSmith DSL from http://www.breminor.com/id93.htm :
11-25,[art2] worth {3d6*10} gold pieces
    on 11-25, roll on art2 table, and the item is worth 3d6*10 gp

1-70,+1 [3E_RangedWeapons.Start] - [MinorWeaponAbilities]
    on 1-70, a +1 ranged weapon with an entry from MinorWeaponAbilities
71-85,+2 [3E_RangedWeapons.Start]
    on 71-85, a +2 ranged weapon from RangedWeapons table.

98-100,[RS~MajorWeaponAbilities2] and [MajorWeaponAbilities2]
    on 98-100, roll twice on MajorWeaponAbilities2, rerolling if they are the same.
101-110,
_[Large Chest]
_[Small Chest]
_[Large Sack]
_[Small Sack]
_[Pouch]
_[Loose Coins=1]
    _ Character is for continuation of previous line (might also add whitespace?)

Variables:
- Uses |x=12| syntax to set variable and not output the string
- can chain multiple, like  |x=12||y=13|
- Reference a variable with % notation:  |%x%>12?[Subtable]/[Subtable2]
-- This can happen in a bare string, like 1-10, %x% gold pieces

15-29,|CP={1d6*10}|[3E_Coin Converter.Start(%CP%)]
    on a 15-29, 1d6*10 copper, which is passed as a parameter to the Coin Converter.Start table.
%itemtype%,0
    initialize variable itemtype to 0
@itemtype,1,Enter the types of items you want generated,Normal Only,Psionic Only,Normal and Psionic
    Variable, Default of 1?, Label, label for 1, label for 2, etc
@ItemType,1, 1-mS, 2-mA, 3-mdS, 4-mdA, 5-MS, 6-MA, 7-Random
    Alternative syntax:
    Variable, Default of 1, 1 is mS, 2 is mA, etc
1,[%itemtype% =0 ? Instructions/DisplayTreasure]
    if itemtype is 0, show Instructions, else show DisplayTreasure
|coins-%x%|
    Substract x from coins.  "coins -= x"

Control flow:
;Level9
    Label for a subtable, reference via [Level9]
[Quantity=%ShopSize%]
    Call the Quantity subtable, using ShopSize as the roll (non-random)
2-10,|y=%coins%||z=%y%|[%y%>7000?LC=1/LC=2]
    set y and z, test y > 700, if true go to subtable LC with roll of 1, else roll of 2 (non-random flow)

"""
from tables.models import Table
#from diceroll.parser import dicerollexpression, expressions as diceexpressions
from diceroll.parser import expr as dice_expression
dice_expression = dice_expression.setResultsName('dice')
import pyparsing
import string

# Dice parsing:
nested_dice_expression = pyparsing.nestedExpr(opener='{', closer='}', content=dice_expression).setResultsName('nested_dice')

def nested_dice_action(s, l, token):
    """
    Rolls dice contained in the nested expression and sums them if
    they haven't already been due to an operator.
    """
    res = token[0].dice.evaluate()
    if isinstance(res, int):
        return res
    return sum(res)

nested_dice_expression.setParseAction(nested_dice_action)

# Table parsing:
def table_action(s, l, token):
    # TODO: This should look up the table by name and get an entry from it.
    return token[0]

# Table names can contain any alphanumeric, spaces, plus a few others
table_name_expression = pyparsing.Word(pyparsing.alphanums + ' _.')

# Full table expressions can include nested dice, e.g. [{3d6} Gems]
table_expression = pyparsing.Forward(pyparsing.Optional(nested_dice_expression) + table_name_expression)
table_expression.setParseAction(table_action)
nested_table_expression = pyparsing.nestedExpr(opener='[', closer=']', content=table_expression).setResultsName('nested_table')
nested_table_expression = nested_table_expression.setResultsName('nested_table')
nested_table_expression.setParseAction(lambda t: t[0])
nested_table_expression.debug = True
# Text is everything else
text = pyparsing.Word(string.printable, excludeChars='{}[]')
text.setParseAction(lambda t: t[0])

full_line = pyparsing.OneOrMore(text | nested_dice_expression | nested_table_expression) + pyparsing.stringEnd

class TableNotFound(Table.DoesNotExist):

    @staticmethod
    def raise_from_path(path):
        raise TableNotFound("Failure loading table %s" % ".".join(path))

def render_entry(entry):
    """
    Extracts tokens from string and calls appropriate rendering methods.
    """
    parts = full_line.parseString(entry)
    return ' '.join(str(part).strip() for part in parts)

def get_table_by_dotted_name(table_name):
    """
    Given a namespaced table name, return the corresponding Table object.

    If any table in the path is not found, TableNotFound is raised with the
    path to the bad table.
    
    E.g. if the path is good.good.bad.bad.bad, the raised exception
    will contain good.good.bad, raised at the first missing table.
    """
    names = table_name.split('.')
    path = []
    table = None
    while names:
        path.append(names.pop(0))
        try:
            table = Table.objects.get(name=path[-1], parent=table)
        except Table.DoesNotExist:
            TableNotFound.raise_from_path(path)
    return table
