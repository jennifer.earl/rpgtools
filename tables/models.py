import random

from django.db import models
from django.db.models.aggregates import Sum
from django.core.exceptions import ImproperlyConfigured
from django.utils.functional import cached_property
from autoslug import AutoSlugField

from tables.signals import auto_add_check_unique_together
from django.contrib.auth.models import User


class Table(models.Model):
    name = models.CharField(blank=False, null=False, max_length=256, db_index=True)
    parent = models.ForeignKey('self', null=True, related_name='children',
                               help_text='Parent of this table, to aid in cross-table refs')
    owner = models.ForeignKey(User, null=True,
                              help_text='Creator or owner of the table.  None means global.')
    slug = AutoSlugField(populate_from='name', unique=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        unique_together = [('name', 'parent'),]

    def get_random_entry(self):
        """
        Picks a random enabled Entry from the table, accounting for weights.
        """
        return random.choice(self.entries_by_weight)

    def get_random_entry_db(self):
        """
        Pick a random entry using the weight_floor and weight_ceiling from the db.
        Should be significantly faster than get_random_entry().
        """
        roll = random.randint(1, self.total_weight)
        entries = self.entries.filter(enabled=True)
        selected_entries = entries.filter(weight_floor__lte=roll,
                                          weight_ceiling__gte=roll)
        if selected_entries.count() > 1:
            raise ImproperlyConfigured("Table entries overlap")
        return selected_entries.first()

    @cached_property
    def total_weight(self):
        entries = self.entries.filter(enabled=True)
        total_weight = entries.aggregate(total_weight=Sum('weight'))['total_weight']
        return total_weight

    @cached_property
    def entries_by_weight(self):
        """
        Returns a list of entries, with each duplicated `weight` times.
        Good for use with random.choice().
        """
        result = list()
        for entry in self.entries.filter(enabled=True):
            result.extend([entry] * entry.weight)
        return result

    def __unicode__(self):
        return "Table {name} {pk}".format(self.__dict__)

    def recalculate_weights(self):
        self.entries.all().update(weight_floor=0, weight_ceiling=0)
        for entry in self.entries.all():
            entry.calculate_weights()
            entry.save(calculate_weights=False)


class TableEntry(models.Model):
    """
    A line item in a table.
    `weight` should be specified by the user, `weight_floor` and `weight_ceiling` are calculated
        automatically on save().
    """
    table = models.ForeignKey(Table, null=False, on_delete=models.CASCADE, related_name='entries')
    text = models.TextField()
    weight = models.IntegerField(default=1)
    enabled = models.BooleanField(default=True)
    display_order = models.IntegerField(default=0)
    weight_floor = models.IntegerField(default=0)
    weight_ceiling = models.IntegerField(default=0)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __repr__(self):
        return u"TableEntry for {table}: {text} {weight} ({pk})".format(table=self.table,
                                                                        text=self.text,
                                                                        weight=self.weight,
                                                                        pk=self.pk)

    def render(self):
        """
        Hook for parsing contents of `text` to allow sub-rolling, etc.
        """
        return self.text

    def calculate_weights(self):
        max_entry = TableEntry.objects.filter(table=self.table).order_by('-weight_ceiling').first()
        if not max_entry:
            max_weight = 0
        else:
            max_weight = max_entry.weight_ceiling
        self.weight_floor = max_weight + 1
        self.weight_ceiling = self.weight_floor + self.weight - 1
    
    def save(self, calculate_weight=True, *args, **kwargs):
        """
        Every time a TableEntry is created or updated, we recalculate some or all of
        the TableEntry's weight_floor and weight_ceiling, to make
        lookups by integer possible.
        """
        if calculate_weight:
            dirty = False
            if not self.pk:
                self.calculate_weights()
            else:
                original = TableEntry.objects.get(pk=self.pk)
                if self.weight != original.weight:
                    dirty = True  # We'll need to regen the whole table
        super().save(*args, **kwargs)

        if calculate_weight and dirty:
            self.table.recalculate_all_weights()

# Connect signal handler to enforce unique_together on SQLITE
auto_add_check_unique_together(model_class=Table)