from django.contrib.auth.models import User
from tastypie import fields
from tastypie.authorization import DjangoAuthorization
from tastypie.resources import ModelResource, ALL, ALL_WITH_RELATIONS
from tables.models import Table, TableEntry
from django.conf.urls import url
from tastypie.utils.urls import trailing_slash
from django.shortcuts import get_object_or_404


class TableResource(ModelResource):
    parent = fields.ForeignKey('self', 'parent', null=True)

    class Meta:
        queryset = Table.objects.all()
#        authorization = DjangoAuthorization()
        resource_name = 'tables'
        list_allowed_methods = ['get', 'post']
        detail_allowed_methods = ['get', 'post', 'put',]  # TODO: Delete with auth
        filtering = {
            'slug': ALL,
            'owner': ALL_WITH_RELATIONS,
            'parent': ALL_WITH_RELATIONS,
            'created': ['exact', 'range', 'gt', 'gte', 'lt', 'lte'],
        }


class TableEntryResource(ModelResource):
    table = fields.ForeignKey(TableResource, 'table')
    
    class Meta:
        resource_name = 'table_entries'
        queryset = TableEntry.objects.all()
#        authorization = DjangoAuthorization()
        list_allowed_methods = ['get', 'post']
        detail_allowed_methods = ['get', 'post', 'put', 'delete']
        filtering = {
            'table': ALL_WITH_RELATIONS,
            'weight_floor': ['exact', 'range', 'gt', 'gte', 'lt', 'lte'],
            'weight_ceiling': ['exact', 'range', 'gt', 'gte', 'lt', 'lte'],
        }

    def prepend_urls(self):
        """
        Add urls of the form:
        /api/v1/tables/{table_slug}/table_entries
        """
        table_resource = TableResource()
        kwargs = {
                  'ts': trailing_slash,
                  'resource_name': self._meta.resource_name,
                  'detail_uri_name': self._meta.detail_uri_name,
                  'table_base_url': r"^{parent_resource_name}/(?P<table>.*?)".format(
                        parent_resource_name=table_resource._meta.resource_name
                        )
                  }
        return [
            url(r"{table_base_url}/(?P<resource_name>{resource_name}){ts}$".format(**kwargs),
                self.wrap_view('dispatch_list'), name="api_dispatch_list"),
            url(r"{table_base_url}/{resource_name}/(?P<{detail_uri_name}>.*?){ts}$".format(**kwargs),
                self.wrap_view('dispatch_detail'), name="api_dispatch_detail"),
                ]

    def get_list(self, request, table=None, *args, **kwargs):
        """
        Catch the table slug from the nested url and turn it into a filter.
        """
        if table:
            kwargs['table'] = get_object_or_404(Table, slug=table)
        return super(TableEntryResource, self).get_list(request, *args, **kwargs)
