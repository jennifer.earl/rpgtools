from django.conf import settings

if 'postgres' in settings.DATABASES['default']['ENGINE']:
    from django.contrib.postgres.fields.jsonb import JSONField
else:
    from jsonfield import JSONField