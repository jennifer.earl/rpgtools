# urls.py
from django.conf.urls import url, include
from party.api import CombatantResource, CombatResource
from tastypie.api import Api



api = Api('combat')
api.register(CombatResource())
#api.register(TableResource())

urlpatterns = [
    url(r'', include(api.urls)),
]