from django.core.management.base import BaseCommand
from IPython.terminal.embed import InteractiveShellEmbed

from party import models


class Command(BaseCommand):
    """
    Interactive shell for managing characters, hidden rolls and combats.
    """
    def add_arguments(self, parser):
        parser.add_argument('--game', type=str, nargs=1, help='Game name or ID; defaults to latest in db.')
        parser.add_argument('--party', type=str, nargs=1, help='Party name or ID; defaults to latest for --game')
        parser.add_argument('--combat', type=str, nargs=1, help='Combat ID; defaults to latest for --party or --game')

    def _create_stdout(self):
        # create a global stdout / stderr object to enable mocking during tests
        global stdout
        stdout = self.stdout
        global stderr
        stderr = self.stderr
    
    def handle(self, *args, **options):
        self._create_stdout()

        _create_proxy_models()
        game = self.load_game(options.get('game', None))
        party = self.load_party(options.get('party', None), game=game)
        party.load_members()
        combat = self.load_combat(options.get('combat', None), game=game)

        print_stdout('Public URL:', combat.get_public_url())
        print_stdout('DM URL:', combat.get_dm_url())
                
        ipshell = InteractiveShellEmbed(banner1='RPG Tools Shell; "help" for assistance.')
        ipshell.Completer.greedy = True  # allow digging into objects
        ipshell.autocall = 2
        ipshell()

    def load_game(self, game_identifier):
        global game
        if game_identifier is None:
            game = Game.objects.latest()
            print_stdout("Loaded default game: %s" % game)
            return game
        try:
            game = Game.objects.get(pk=game_identifier)
            print_stdout("Loaded game by PK: %s" % game)
            return game
        except (Game.DoesNotExist, ValueError):
            pass
        try:
            game = Game.objects.get(name=game_identifier)
            print_stdout("Loaded game by name: %s" % game)
            return game
        except Game.DoesNotExist:
            raise Game.DoesNotExist("Couldn't find a game matching: %s" % game_identifier)

    def load_party(self, party_identifier, game):
        global party
        if party_identifier is None:
            party = Party.objects.filter(game=game).latest()
            print_stdout("Loaded latest party for game %s: %s" % (game, party))
            return party
        try:
            party = Party.objects.filter(game=game).get(pk=party_identifier)
            print_stdout("Loaded party by ID: %s" % party)
            return party
        except (Party.DoesNotExist, ValueError):
            pass
        try:
            party = Party.objects.filter(game=game).get(name=party_identifier)
            print_stdout("Loaded party by name: %s" % party)
            return party
        except Party.DoesNotExist:
            raise Party.DoesNotExist("Couldn't find a party matching: %s" % party_identifier)

    def load_combat(self, combat_identifier, game):
        global combat
        if combat_identifier is None:
            try:
                combat = Combat.objects.filter(game=game).latest()
                print_stdout("Loaded latest combat for game %s: %s" % (game, combat))
                return combat
            except Combat.DoesNotExist:
                print_stdout("No default combat for game %s, creating a new empty one." % game)
                combat = Combat.objects.create(game=game)
                return combat
        try:
            combat = Combat.objects.filter(game=game).get(pk=combat_identifier)
            print_stdout("Loaded combat by ID: %s" % combat)
            return combat
        except Combat.DoesNotExist:
            raise Combat.DoesNotExist("Couldn't find a combat matching: %s" % combat)


def print_stdout(*args):
    """
    Mockable wrapper for stdout that behaves a little like print().
    """
    global stdout
    stdout.write("\t".join([str(arg) for arg in args]))


def help():  # noqa
    """
    Prints help text.
    """
    print_stdout("Global Objects")
    print_stdout("==============")
    print_stdout("The shell maintains references to several useful objects:")
    print_stdout("party: the last party loaded")
    print_stdout("game: the game belonging to party, or last game loaded")
    print_stdout("")
    print_stdout("Classes")
    print_stdout("=======")
    print_stdout("Most Django ORM objects are exposed:")
    print_stdout("Party - for loading / saving an entire party; use 'party' to interact with the current party")
    print_stdout("")
    print_stdout("Commands")
    print_stdout("========")
    print_stdout("In addition to Django ")
    print_stdout("party: display information about current party")
    print_stdout("Party.load: loads a party from database; takes optional name of party")
    print_stdout("party.roll skillname: rolls `skillname` for all members of the party, prompting for the skill if missing.")


def _create_proxy_models():
    # Wrappers for django ORM objects, to manage global state within the shell
    # e.g. Party.load automatically sets the global 'party' object
    global Creature
    class CreatureProxy(models.Creature):
        objects = models.Creature.objects
        def roll(self, stat):
            """
            Wrapper for Creature.roll(), to allow prompting for stats in the CLI.
            """
            if stat not in self.stats:
                prompt = "{NAME}'s {STATNAME}: ".format(NAME=self.name, STATNAME=stat)
                res = raw_input_no_history(prompt)
                if res:
                    self.set_stat(stat, res)
                else:
                    print_stdout("Skipping {NAME}".format(NAME=self.name))
            result = super().roll(stat)

            # TODO: Make this configurable for non-d20
            if result is not None and result - self.get_stat(stat) == 20:
                print_stdout("* {NAME} rolled a natural 20 for {STAT}".format(NAME=self.name,
                                                                              STAT=stat))
            return super().roll(stat)

        def __repr__(self):
            """
            Useful output from typing a creature name in the shell.
            @TODO: load a stat block formatter and use that.
            """
            _stats = self.previous_stats.copy()
            _stats.update(self.stats)
            lines = ["{STAT}: {AMOUNT}".format(STAT=k, AMOUNT=v)
                        for k,v in _stats.items()]
            lines.sort()
            lines = "\n".join(lines)
            sep = '-' * len(self.name)
            return "{NAME}\n{SEP}\n{LINES}".format(NAME=self.name,
                                                   SEP=sep,
                                                   LINES=lines)

        class Meta:
            proxy = True
    Creature = CreatureProxy

    global Party
    class PartyProxy(models.Party):
        objects = models.Party.objects

        def load_members(self):
            """
            Populates self.pcs with CreatureProxy objects, to add CLI behavior.
            Stores NPCs and PCs in self.pcs - @TODO fix this, npcs should go elsewhere but behave the same.
            """
            self.pcs = CharacterPool()
            for character_id in self.saved_characters.values_list('pk', flat=True):
                character = CreatureProxy.objects.get(pk=character_id)
                setattr(self.pcs, character.name, character)

        def add_character(self, name, type='PC'):  # noqa
            """
            Loads an existing character/creature from the database and adds it to the party.
            """
            if type not in ('PC', 'NPC'):
                print_stdout("party.add_character() is for PCs or NPCs; use combat.add_monster")
            kwargs = {'type': type,
                      'name': name}
            if type in ('PC', 'NPC'):
                kwargs['game'] = self.game
                kwargs['party'] = self
            character = Creature.objects.get(**kwargs)
            if type in ('PC', 'NPC'):
                setattr(self.pcs, character.name, character)
        
            return character

        def roll(self, stat):
            """
            Rolls a stat for every character in the party.
            """
            rolls = [{'NAME': c.name,
                     'ROLL': c.roll(stat)}
                        for c in self.pcs]
            for roll in rolls:
                print_stdout("{NAME}:\t{ROLL}".format(**roll))
    

        class Meta:
            proxy = True
    Party = PartyProxy

    global Game
    class GameProxy(models.Game):
        """
        Wrapper for Game object to enable CLI behaviors.
        """
        
        objects = models.Game.objects

        def __str__(self):
            return "<Game pk={PK} name={NAME}>".format(PK=self.pk, NAME=self.name)
        class Meta:
            proxy = True
    Game = GameProxy
    
    global Combat
    class CombatProxy(models.Combat):
        """
        Wrapper for Combat object to enable CLI behaviors.
        """
        def __str__(self):
            res = "Combat:\n-------"
            if self.positions:
                combatants = self.combatants_by_position
                for combatant in combatants:
                    res += "\n{NAME}: {INITIATIVE}".format(NAME=combatant.name,
                                                           INITIATIVE=combatant.initiative)
            else:
                combatants = self.combatants.order_by('name')
                for combatant in combatants:
                    res += "\n{NAME}".format(NAME=combatant.name)
            return res
        
        class Meta:
            proxy = True

        def clear(self):
            """
            Override base 'clear' to automatically pass global party object.
            """
            global party
            return super().clear(party)

        def add(self, what, type=None, count=1, add_late=None):  # noqa
            """
            Adds an arbitrary creature or collection to the combat.
            Usage: 
              combat.add('orc', type='monster', count=5)
              combat.add(party)
              combat.add(Creature.objects.create(name='Bob', type='PC')
            """
            if add_late is None and self.started:
                res = raw_input_no_history("Combat already started; move high initiative to now? (Y/n)")
                add_late = bool(res in ('y', 'Y', ''))

            if isinstance(what, models.Party):
                if hasattr(what, 'pcs'):
                    for pc in what.pcs:
                        print_stdout("Adding PC to combat:", pc.name)
                        super().add(pc, type='PC', add_late=add_late)
                else:
                    for pc_id in what.saved_characters.values_list('pk', flat=True):
                        pc = Creature.objects.get(pk=pc_id)
                        print_stdout("Adding PC to combat:", pc.name)
                        super().add(pc, type='PC', add_late=add_late)
            elif isinstance(what, Combat):  # add a combat to another combat
                for combatant in what.combatants.all():
                    self.add(combatant.creature, combatant.creature.type, add_late=add_late)
            else:
                # fallback case, treat it as a creature
                return super().add(what, type=type, count=count, add_late=add_late)

        def add_monster(self, name, count=1):
            """
            Loads or creates a monster and adds it to the combat.
            """
            try:
                monster = Creature.objects.get(name=name, game=self.game, type='monster')
            except Creature.DoesNotExist:
                res = raw_input_no_history("No such creature; create a '%s'?" % name)
                if res.lower() in ('y', 'yes'):
                    monster = Creature.objects.create(name=name, game=self.game, type='monster')
                    monster.roll_initiative()  # to trigger input for initiative stat
                else:
                    return False

            add_late = False
            if self.started:
                res = raw_input_no_history("Combat already started; move high initiative to now? (Y/n)")
                add_late = bool(res in ('y', 'Y', ''))
        
            return super().add(monster, type='monster', count=count, add_late=add_late)

        @property
        def combatants(self):
            """
            Override models.combat.Combat.combatants with CombatantProxy queryset.
            """
            return CombatantProxy.objects.filter(combat=self)

    global Combatant
    class CombatantProxy(models.Combatant):

        @property
        def creature(self):
            """
            Override models.combat.Combatant.creature with CreatureProxy object
            """
            return CreatureProxy.objects.get(pk=super().creature.pk)

        class Meta:
            proxy = True
    
    Combat = CombatProxy
    Combatant = CombatantProxy


def raw_input_no_history(output=None):
    """
    Get input from console, and strike it from readline's history so up-arrow doesn't repeat it.
    """
    import readline
    print_stdout(output)
    res = input()
    current_history_length = readline.get_current_history_length()
    if current_history_length > 0:
        readline.remove_history_item(readline.get_current_history_length()-1)
    return res
    

class CharacterPool(object):
    """
    A collection of characters; used to enable tab-completion in ipython.
    """
    def __iter__(self):
        """
        Allow iteration over contents.
        TODO: sort
        """
        for k in self.__dict__.keys():
            yield getattr(self, k)

def ready(reason=None):
    """
    Mark current combatant as ready, with an optional reason.
    """
    global combat
    if not combat.current_turn:
        raise RuntimeError("Combat has not started, or nobody is involved.")
    combat.current_turn.is_ready = True
    combat.current_turn.ready_reason = reason
    combat.current_turn.save(update_fields=('is_ready', 'ready_reason'))
    return combat.advance_turn()


def unhide():
    """
    Unhide the current combatant.
    """
    global combat
    if not combat.current_turn:
        raise RuntimeError("Combat has not started, or nobody is involved.")
    if not combat.current_turn.is_hidden:
        print_stdout(combat.current_turn, "is already visible.")
        return
    combat.current_turn.mark_visible()
show = unhide  # synonym


def kill(name=None, type=None, **kwargs):  # noqa
    """
    Marks a combatant dead.  If name/type not specified, kills current turn.
    """
    global combat

    if name:
        combatant = combat.get_one_combatant_by_filters(name=name, type=type, **kwargs)
    else:
        if not combat.current_turn:
            raise RuntimeError("Combat has not started, or nobody is involved.")
        combatant = combat.current_turn
    
    if combatant.is_dead:
        print_stdout(combat.current_turn, "is already dead")
    combatant.mark_dead()

mark_dead = kill  # synonym


def ready_trigger(name, type=None, **kwargs):  # noqa
    """
    Moves a readied character to the current turn and makes it their turn.
    Specify name / type, of combatant or creature, or pass in a creature object (`party.pcs.Bob`)

    TODO: This probably belongs in the Combat class
    """
    global combat
    # get from args to combatant
    # TODO:  use Combat.get_combatant_by_filter et al
    if isinstance(name, models.Creature):
        combatant = Combatant.objects.get(creature=name, is_ready=True)  # will error if more than one match
    elif isinstance(name, models.Combatant):
        combatant = name
        if not combatant.is_ready:
            print_stdout(combatant, "is not readied")
            return False
    else:
        combatant = combat.get_one_combatant_by_filters(name=name, type=type, is_ready=True)

    combat.move_combatant_before(combatant, combat.current_turn)
    combatant.is_ready = False
    combatant.ready_reason = None
    combatant.save(update_fields=('is_ready', 'ready_reason'))
    return combat.current_turn
trigger = ready_trigger  # synonym
