from django.test import TestCase

from ..models import Combat, Combatant, Game, Creature
from django.urls.base import reverse
from django.core.exceptions import MultipleObjectsReturned
from bs4 import BeautifulSoup


class TestCombatAPI(TestCase):
    def setUp(self):
        self.game = Game.objects.create()
        self.game.set_secret_key()
        
        self.pc = Creature(name='PC', type='PC', game=self.game)
        self.pc.stats = {'Initiative': 40}  # force PC to win init rolls
        self.pc.save()

        self.goblin = Creature(name='Goblin', type='monster', game=self.game)
        self.goblin.stats = {'Initiative': 0}
        self.goblin.save()
        
        self.combat = Combat.objects.create(game=self.game)
        self.combat.add(self.pc)
        self.combat.add(self.goblin)

        # some more Bestiary
        self.orc = Creature.objects.create(name='Orc', type='monster', game=self.game, stats={'Initiative': -20})
        self.dragon = Creature.objects.create(name='Dragon', type='monster', game=self.game,
                                              stats={'Initiative': 60})

    def _add_more_combatants(self):
        """
        Populate the combat with a larger number of combatants:
        - one more goblin
        - five orcs
        - one dragon
        """
        self.combat.add(self.goblin)
        self.combat.add(self.orc, count=5)
        self.combat.add(self.dragon)

    def test_get_combat_detail_view(self):
        self.combat.start()
        res = self.client.get(self.combat.get_api_detail_url())
        self.assertEqual(res.status_code, 200)
        body = res.json()

        self.assertEqual(body['current_round'], 0)

        res_combatants = body['combatants']
        self.assertEqual(len(res_combatants), 2)

        self.assertEqual(res_combatants[0]['name'], 'PC')
        self.assertEqual(res_combatants[1]['name'], 'Goblin')

        PC = res_combatants[0]
        self.assertTrue('is_ready' in PC)
        self.assertEqual(PC['is_ready'], False)

    def test_get_combats(self):
        self.combat.start()
        url = reverse('api_dispatch_list', kwargs={'resource_name': 'combats', 'api_name': 'combat'})
        res = self.client.get(url)
        self.assertEqual(len(res.json()['objects']), 1)

    def test_get_combatants_hidden(self):
        """
        A request to the dispatch list view without the game's secret key should not return hidden
        combatants.
        """
        self._add_more_combatants()
        self.combat.mark_all_hidden('Goblin')
        self.combat.start()

        res = self.client.get(self.combat.get_api_detail_url())
        self.assertEqual(res.status_code, 200)
        body = res.json()
        res_combatants = body['combatants']
        self.assertEqual(len(res_combatants), 7)
        for c in res_combatants:
            self.assertTrue('Goblin' not in c['name'])

        # Re-do the request with the secret key provided
        res = self.client.get(self.combat.get_api_detail_url(), {'sk': self.game.secret_key})
        self.assertEqual(res.status_code, 200)
        body = res.json()
        res_combatants = body['combatants']
        self.assertEqual(len(res_combatants), 9)  # two hidden goblins included

        combatant_names = [c['name'] for c in res_combatants]
        self.assertEqual(combatant_names.count('Goblin'), 2)  # goblins have not been numbered yet

    def test_get_current_combatant_hidden(self):
        """
        When a player requests the combat, the current combatant should 'stick' to the last unhidden one
        if the actual current turn is a hidden combatant.
        """
        self._add_more_combatants()
        self.combat.mark_hidden('PC')
        self.combat.start()
        self.combat.advance_turn()  # it should now be the PC's turn
        self.assertEqual(self.combat.current_turn.creature, self.pc)

        res = self.client.get(self.combat.get_api_detail_url())
        self.assertEqual(res.status_code, 200)
        body = res.json()
        res_current_turn = body['current_turn']
        self.assertEqual(res_current_turn['name'], 'Dragon')

        # but if we GET with the secret key, it's the PC's turn
        res = self.client.get(self.combat.get_api_detail_url(), {'sk': self.game.secret_key})
        self.assertEqual(res.status_code, 200)
        body = res.json()
        res_current_turn = body['current_turn']
        self.assertEqual(res_current_turn['name'], 'PC')

    def test_get_current_combatant_hidden_first_combatant(self):
        """
        The logic of getting "the combatant before this one that isn't hidden" is pretty complex.
        Normally the very first combatant in a combat being hidden won't last very long.
        """
        self._add_more_combatants()
        self.combat.mark_hidden('Dragon')
        self.combat.current_round = 5  # we might do something special if it's round 0
        self.combat.save()
        self.combat.start()
        
        res = self.client.get(self.combat.get_api_detail_url())
        self.assertEqual(res.status_code, 200)
        body = res.json()
        res_current_turn = body['current_turn']
        self.assertTrue(res_current_turn['name'] in ('Orc #1', 'Orc #2', 'Orc #3', 'Orc #4', 'Orc #5'))

        # but if we GET with the secret key, it's the Dragon's turn
        res = self.client.get(self.combat.get_api_detail_url(), {'sk': self.game.secret_key})
        self.assertEqual(res.status_code, 200)
        body = res.json()
        res_current_turn = body['current_turn']
        self.assertEqual(res_current_turn['name'], 'Dragon')

    def test_get_current_combatant_all_hidden(self):
        """
        Edge case where everybody in combat is hidden.
        """
        self.combat.mark_hidden('Goblin')
        self.combat.mark_hidden('PC')
        self.combat.current_round = 5  # we might do something special if it's round 0
        self.combat.save()
        self.combat.start()
        
        res = self.client.get(self.combat.get_api_detail_url())
        self.assertEqual(res.status_code, 200)
        body = res.json()
        res_current_turn = body['current_turn']
        self.assertEqual(res_current_turn, None)
        
        # but if we GET with the secret key, it's the PC's turn
        res = self.client.get(self.combat.get_api_detail_url(), {'sk': self.game.secret_key})
        self.assertEqual(res.status_code, 200)
        body = res.json()
        res_current_turn = body['current_turn']
        self.assertEqual(res_current_turn['name'], 'PC')

