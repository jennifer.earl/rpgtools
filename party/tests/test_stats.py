from django.test import TestCase

from ..models import StatAlias, CanonicalStat, Game


class StatAliasTestCase(TestCase):
    """
    Tests for various means of finding stats.
    """

    def setUp(self):
        self.game = Game.objects.create()
        self.arcane = CanonicalStat.objects.create(canonical_stat_name='Knowledge (Arcane)', game=self.game)
        self.arcane_alias_one = StatAlias.objects.create(canonical_stat=self.arcane,
                                                         alias='Arcane')
        self.arcane_alias_two = StatAlias.objects.create(canonical_stat=self.arcane,
                                                         alias='Know (Arcane)')

    def test_save_digested(self):
        stat = CanonicalStat.objects.create(canonical_stat_name='Craft (Arms and Armor)', game=self.game)
        self.assertEqual(stat.canonical_stat_name_digested, 'craft arms and armor')

    def test_save_alias_digested(self):
        alias = StatAlias.objects.create(canonical_stat=self.arcane,
                                         alias='Knowledge (Arcana)')
        self.assertEqual(alias.alias_digested, 'knowledge arcana')

    def test_get_canonical_by_name(self):
        self.assertEqual(StatAlias.get_canonical_stat(alias_string='Knowledge (Arcane)', game=self.game),
                         self.arcane)

    def test_get_canonical_by_digested_name(self):
        self.assertEqual(StatAlias.get_canonical_stat(alias_string='knowledge arcane', game=self.game),
                         self.arcane)

    def test_get_canonical_by_alias(self):
        self.assertEqual(StatAlias.get_canonical_stat(alias_string='arcane', game=self.game),
                         self.arcane)

    def test_get_canonical_by_alias_degested(self):
        self.assertEqual(StatAlias.get_canonical_stat(alias_string='know arcane', game=self.game),
                         self.arcane)

    def test_get_canonical_not_found(self):
        self.assertEqual(StatAlias.get_canonical_stat(alias_string='monkey', game=self.game),
                         None)
