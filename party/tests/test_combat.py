from django.test import TestCase

from ..models import Combat, Combatant, Game, Creature
from django.urls.base import reverse
from django.core.exceptions import MultipleObjectsReturned
from bs4 import BeautifulSoup


class TestInitiative(TestCase):
    def setUp(self):
        self.game = Game.objects.create()
        self.game.set_secret_key()
        
        self.pc = Creature(name='PC', type='PC', game=self.game)
        self.pc.stats = {'Initiative': 40}  # force PC to win init rolls
        self.pc.save()

        self.goblin = Creature(name='Goblin', type='monster', game=self.game)
        self.goblin.stats = {'Initiative': 0}
        self.goblin.save()
        
        self.combat = Combat.objects.create(game=self.game)
        self.combat.add(self.pc)
        self.combat.add(self.goblin)

        # some more Bestiary
        self.orc = Creature.objects.create(name='Orc', type='monster', game=self.game, stats={'Initiative': -20})
        self.dragon = Creature.objects.create(name='Dragon', type='monster', game=self.game,
                                              stats={'Initiative': 60})

        
    def _load_combat_page(self, dm_view=False):
        combat_page_url = reverse('player_combat_view_by_id', kwargs={'combat_id': self.combat.pk})
        params = {}
        if dm_view:
            params['sk'] = self.combat.game.secret_key
        return self.client.get(combat_page_url, params)

    def _get_combat_soup(self, dm_view=False):
        """
        Loads the combat page and parses it into a BeautifulSoup queryable instance.
        """
        res = self._load_combat_page(dm_view=dm_view)
        return BeautifulSoup(res.content, 'html.parser')

    def _add_more_combatants(self):
        """
        Populate the combat with a larger number of combatants:
        - one more goblin
        - five orcs
        - one dragon
        """
        self.combat.add(self.goblin)
        self.combat.add(self.orc, count=5)
        self.combat.add(self.dragon)

    def _get_combatant_names_by_position(self):
        """
        Returns a list of creature names (without numbering) in initiative order.
        """
        return [Combatant.objects.get(pk=pk).creature.name for pk in self.combat.positions]

    def test_roll_initiative(self):
        self.combat.initialize()
        self.assertEqual(self.combat.positions,
                         [self.pc.pk, self.goblin.pk])

    def test_get_monsters_by_name(self):
        self._add_more_combatants()
        monster_bin = self.combat._monsters_by_name
        self.assertEqual(len(monster_bin['Dragon']), 1)
        self.assertEqual(monster_bin['Dragon'][0].creature, self.dragon)

        self.assertEqual(len(monster_bin['Goblin']), 2)
        for combatant in monster_bin['Goblin']:
            self.assertEqual(combatant.creature, self.goblin)

        self.assertEqual(len(monster_bin['Orc']), 5)
        for combatant in monster_bin['Orc']:
            self.assertEqual(combatant.creature, self.orc)

        # PC is not in the monsters by name
        self.assertEqual(len(monster_bin.keys()), 3)

    def test_renumber_monsters(self):
        combatants = self.combat.add(self.orc, count=5)
        self.combat.renumber_monsters(combatants)
        orc_combatants = self.combat.combatants.filter(creature=self.orc)
        self.assertEqual([combatant.name for combatant in orc_combatants],
                         ['Orc #1',
                          'Orc #2',
                          'Orc #3',
                          'Orc #4',
                          'Orc #5',
                          ])
        
    def test_fix_monster_names(self):
        """
        Test the whole shebang.
        """
        self._add_more_combatants()
        self.combat.fix_monster_names()

        self.assertEqual(['Dragon',
                          'Goblin #1', 'Goblin #2',
                          'Orc #1', 'Orc #2', 'Orc #3', 'Orc #4', 'Orc #5',
                          'PC'],
                         list(self.combat.combatants.order_by('name').values_list('name', flat=True)))

    def test_fix_monster_names_skip_hidden(self):
        """
        Hidden monsters shouldn't be included in the renumbering, but should be numbered upon becoming
        visible.
        """
        self._add_more_combatants()

        # Mark alternate Orcs as hidden
        mark_hidden = True
        for combatant in self.combat.combatants.filter(creature__name='Orc'):
            if mark_hidden:
                combatant.is_hidden = True
                combatant.save()
                mark_hidden = False
            else:
                mark_hidden = True

        # Check scenario:
        self.assertEqual(self.combat.combatants.filter(creature__name='Orc', is_hidden=True).count(),
                         3)
        self.assertEqual(self.combat.combatants.filter(creature__name='Orc', is_hidden=False).count(),
                         2)

        self.combat.fix_monster_names()
        orc_combatants = self.combat.combatants.filter(creature__name='Orc')
        hidden_orc_names = list(orc_combatants.filter(is_hidden=True).values_list('name', flat=True))
        self.assertEqual(hidden_orc_names, ['Orc', 'Orc', 'Orc'])
        visible_orc_names = list(orc_combatants.filter(is_hidden=False)
                                 .order_by('name').values_list('name', flat=True))
        self.assertEqual(visible_orc_names, ['Orc #1', 'Orc #2'])

    def test_mark_all_visible_monster_renumbers(self):
        """
        Marking multiple monsters visible should renumber them correctly.
        """
        self._add_more_combatants()
  
        # TODO: there's no good method to add a single hidden monster
        Combatant.objects.create(creature=self.orc, combat=self.combat, is_hidden=True, name='Orc')
        Combatant.objects.create(creature=self.orc, combat=self.combat, is_hidden=True, name='Orc')
        self.combat.fix_monster_names()

        orc_names = set([c.name for c in self.combat.combatants.filter(creature=self.orc)])
        self.assertEqual(orc_names,
                         set(['Orc #1', 'Orc #2', 'Orc #3', 'Orc #4', 'Orc #5', 'Orc']))

        self.combat.mark_all_visible('Orc')

        orc_names = set([c.name for c in self.combat.combatants.filter(creature=self.orc)])
        self.assertEqual(orc_names,
                         set(['Orc #1', 'Orc #2', 'Orc #3', 'Orc #4', 'Orc #5', 'Orc #6', 'Orc #7']))

    def test_mark_one_visible_monster_renumber(self):
        self._add_more_combatants()
  
        # TODO: there's no good method to add a single hidden monster
        hidden_orc = Combatant.objects.create(creature=self.orc, combat=self.combat, is_hidden=True, name='Orc')
        hidden_orc_2 = Combatant.objects.create(creature=self.orc, combat=self.combat, is_hidden=True, name='Orc')
        self.combat.fix_monster_names()

        orc_names = set([c.name for c in self.combat.combatants.filter(creature=self.orc)])
        self.assertEqual(orc_names,
                         set(['Orc #1', 'Orc #2', 'Orc #3', 'Orc #4', 'Orc #5', 'Orc']))

        # Mark the one hidden orc visible
        self.combat.mark_visible(pk=hidden_orc.pk)
        orc_names = set([c.name for c in self.combat.combatants.filter(creature=self.orc)])
        self.assertEqual(orc_names,
                         set(['Orc #1', 'Orc #2', 'Orc #3', 'Orc #4', 'Orc #5', 'Orc #6', 'Orc']))

        self.combat.mark_visible(pk=hidden_orc_2.pk)
        orc_names = set([c.name for c in self.combat.combatants.filter(creature=self.orc)])
        self.assertEqual(orc_names,
                         set(['Orc #1', 'Orc #2', 'Orc #3', 'Orc #4', 'Orc #5', 'Orc #6', 'Orc #7']))
  

    def test_initialize(self):
        self._add_more_combatants()
        self.combat.initialize()

        self.assertEqual(self.combat.current_turn.creature.name, 'Dragon')

        # after initializing, 'positions' should be the order of initiative;
        # this isn't strictly deterministic, but we should have monsters grouped by type (due to
        # the absurd init stats)

        names = self._get_combatant_names_by_position()
        expected_names = ['Dragon', 'PC', 'Goblin', 'Goblin', 'Orc', 'Orc', 'Orc', 'Orc', 'Orc']
        self.assertEqual(names, expected_names)
        
        # we can also get Combatants by position
        combatant_names = [combatant.creature.name for combatant in self.combat.combatants_by_position]
        self.assertEqual(combatant_names, expected_names)

    def test_advance_turn(self):
        self._add_more_combatants()
        self.combat.initialize()

        # Zip through entire initiative, collecting names
        names = []
        expected_names = ['Dragon', 'PC', 'Goblin', 'Goblin', 'Orc', 'Orc', 'Orc', 'Orc', 'Orc']

        for _ in range(len(expected_names)):
            names.append(self.combat.current_turn.creature.name)
            self.combat.advance_turn()
        
        self.assertEqual(names, expected_names)

        # The last advance brought us back to the beginning
        self.assertEqual(self.combat.current_turn.creature.name, 'Dragon')

    def test_move_combatant_before(self):
        """
        Tests reordering combatants.
        """
        self._add_more_combatants()
        self.combat.initialize()

        self.assertEqual(self.combat.current_turn.creature, self.dragon)

        self.combat.move_combatant_before('PC', 'Dragon')

        self.assertEqual(self.combat.current_turn.creature, self.pc)

        # Zip through entire initiative, collecting names
        names = self._get_combatant_names_by_position()
        expected_names = ['PC', 'Dragon', 'Goblin', 'Goblin', 'Orc', 'Orc', 'Orc', 'Orc', 'Orc']

        self.assertEqual(names, expected_names)

    def test_move_current_combatant_before(self):
        """
        When the current combatant decides to delay to just before somebody else,
        it should stop being their turn.
        """
        self._add_more_combatants()
        self.combat.initialize()
        
        # currently the dragon's turn
        self.assertEqual(self.combat.current_turn.creature, self.dragon)

        # advance to PC's turn
        self.combat.advance_turn()
        self.assertEqual(self.combat.current_turn.creature, self.pc)

        # we move the PC to just before the Dragon
        self.combat.move_combatant_before('PC', 'Dragon')

        # now the current turn should belong to a Goblin
        self.assertEqual(self.combat.current_turn.creature, self.goblin)

    def test_add_late_combatant_renumbers(self):
        self._add_more_combatants()
        self.combat.initialize()

        # expected names is from the Combatant objects, which includes numbering
        expected_names = set(['PC', 'Dragon',
                              'Goblin #1', 'Goblin #2',
                              'Orc #1', 'Orc #2', 'Orc #3', 'Orc #4', 'Orc #5']
                              )

        self.assertEqual(self.combat.combatant_names, expected_names)

        # add another Orc, which we expect to become 'Orc #6'
        self.combat.add(self.orc)

        expected_names.add('Orc #6')
        self.assertEqual(self.combat.combatant_names, expected_names)

    def test_remove_combatant(self):
        """
        Removing a combatant just deletes the Combatant object, but leaves the Creature alone.
        Also stops appearing on combat screen.
        """
        self._add_more_combatants()
        self.combat.initialize()

        goblin_2 = self.combat.combatants.filter(name='Goblin #2').get()
        goblin_creature = goblin_2.creature

        self.assertEqual(self.combat.combatants.filter(creature=goblin_creature).count(),
                         2)
        
        self.combat.remove('Goblin #2')

        goblin_creature.refresh_from_db()

        self.assertEqual(self.combat.combatants.filter(creature=goblin_creature).count(),
                         1)

    def test_load_latest_combat(self):
        """
        Loading the combat page without an ID should redirect to the 'official' ID.
        """
        url = reverse('player_combat_view')
        res = self.client.get(url)
        self.assertEqual(res.status_code, 302)
        expected_url = reverse('player_combat_view_by_id', kwargs={'combat_id': self.combat.pk})
        self.assertEqual(res['Location'], expected_url)
        
    def test_remove_all(self):
        self._add_more_combatants()
        self.combat.initialize()

        self.assertEqual(self.combat.combatants.filter(creature=self.goblin).count(),
                         2)
        
        self.combat.remove_all('Goblin')

        self.goblin.refresh_from_db()

        self.assertEqual(self.combat.combatants.filter(creature=self.goblin).count(),
                         0)

    def test_mark_hidden(self):
        """
        Marking a combatant hidden should remove it from the player view.
        """
        self._add_more_combatants()
        self.combat.initialize()

        self.assertContains(self._load_combat_page(), 'Goblin #2')

        self.combat.mark_hidden('Goblin #2')

        goblin_2 = self.combat.combatants.filter(name='Goblin #2').get()
        self.assertTrue(goblin_2.is_hidden)

        res = self._load_combat_page()

        self.assertNotContains(res, "Goblin #2")

        self.combat.mark_visible('Goblin #2')
        self.assertContains(self._load_combat_page(), 'Goblin #2')

    def test_mark_all_hidden(self):
        """
        Marking all Goblins hidden means they shouldn't be in the player view.
        """
        self._add_more_combatants()
        self.combat.initialize()
    
        res = self._load_combat_page()
        self.assertContains(res, "Goblin #1")
        self.assertContains(res, "Goblin #2")

        self.combat.mark_all_hidden('Goblin')
    
        res = self._load_combat_page()
        self.assertNotContains(res, "Goblin #1")
        self.assertNotContains(res, "Goblin #2")

    def test_mark_hidden_dm_view(self):
        """
        The DM view should show hidden monsters, but with the 'hidden_combatant' class.
        """
        self._add_more_combatants()
        self.combat.initialize()
        self.combat.mark_all_hidden('Goblin')

        soup = self._get_combat_soup(dm_view=True)

        hidden_combatants = soup.find_all('tr', class_='hidden_combatant')
        self.assertEqual(len(hidden_combatants), 2)

    def test_mark_hidden_too_broad(self):
        """
        `mark_hidden` raises an error when trying to apply it to multiple characters.
        """
        self._add_more_combatants()
        self.combat.initialize()

        with self.assertRaises(MultipleObjectsReturned):
            self.combat.mark_hidden('Goblin')

    def test_mark_dead(self):
        """
        Dead things get marked with strikethrough decoration.  We care about the CSS class.
        """
        self._add_more_combatants()
        self.combat.initialize()

        goblin_2 = self.combat.combatants.filter(name='Goblin #2').get()

        soup = self._get_combat_soup()

        self.assertEqual(soup.find_all('tr', class_='dead_combatant'), [])

        self.combat.mark_dead('Goblin #2')

        soup = self._get_combat_soup()
        tags = soup.find_all('tr', class_='dead_combatant')
        self.assertEqual(len(tags), 1)
        tag = tags[0]
        cells = tag.find_all('td')
        cell_texts = [cell.text for cell in cells]  # a list containing the text from each <td>

        self.assertTrue(str(goblin_2.initiative) in cell_texts)
        self.assertTrue('Goblin #2' in cell_texts)

    def test_get_combatants_by_name(self):
        self._add_more_combatants()

        combatants = self.combat.get_combatants_by_name('Goblin')
        self.assertEqual(combatants.count(), 2)
        for combatant in combatants:
            self.assertEqual(combatant.creature, self.goblin)

    def test_get_combatants_by_name_case_insensitive(self):
        """
        In general, we should make queries case insensitive.
        """
        self._add_more_combatants()

        combatants = self.combat.get_combatants_by_name('goblin')
        self.assertEqual(combatants.count(), 2)
        for combatant in combatants:
            self.assertEqual(combatant.creature, self.goblin)

    def test_get_combatants_by_name_and_type(self):
        """
        In the annoying case where a player names a PC after a monster,
        type can filter them out.
        """
        self._add_more_combatants()
        goblin_pc = Creature.objects.create(type='PC', name='Goblin', stats={'Initiative': 8})

        self.combat.add(goblin_pc)

        combatants = self.combat.get_combatants_by_name('goblin', 'monster')
        self.assertEqual(combatants.count(), 2)

        pc_combatants = self.combat.get_combatants_by_name('goblin', 'PC')
        self.assertEqual(pc_combatants.count(), 1)
        pc_combatant = pc_combatants.get()
        self.assertEqual(pc_combatant.creature, goblin_pc)

    def test_get_combatants_by_filters(self):
        """
        Arbitrary filters can be used to find combatants.
        """
        self._add_more_combatants()
        self.combat.initialize()
        slow_pokes = self.combat.get_combatants_by_filters(initiative__lte=0)

        # because Orcs all have -20 initiative, they should range from 0 to -19
        for slow_poke in slow_pokes:
            self.assertTrue(slow_poke.initiative <= 0)
            self.assertEqual(slow_poke.creature, self.orc)

        # Randomly mark some combatants dead, so we can filter for them.
        for combatant in self.combat.combatants.all():
            if combatant.initiative % 2 == 0:
                combatant.is_dead = True
                combatant.save()

        dead_combatants = self.combat.get_combatants_by_filters(is_dead=True)

        for combatant in self.combat.combatants.all():
            if combatant.is_dead:
                self.assertTrue(combatant in dead_combatants)
            else:
                self.assertFalse(combatant in dead_combatants)

    def test_remove_all_clears_positions(self):
        """
        Removing all creatures by name should remove them from the positions list.
        """
        self._add_more_combatants()
        self.combat.initialize()
 
        self.assertEqual(len(self.combat.combatants_by_position), 9)
        self.assertEqual(len(self.combat.positions), 9)
        self.combat.remove_all('Orc')
        self.assertEqual(len(self.combat.combatants_by_position), 4)
        self.assertEqual(len(self.combat.positions), 4)

    def test_remove_clears_position(self):
        """
        Removing one creature by name should remove it from the positions list.
        """
        self._add_more_combatants()
        self.combat.initialize()
 
        self.assertEqual(len(self.combat.combatants_by_position), 9)
        self.assertEqual(len(self.combat.positions), 9)

        self.combat.remove('Orc #1')
        self.assertEqual(len(self.combat.combatants_by_position), 8)
        self.assertEqual(len(self.combat.positions), 8)
        