"""
Tests of the `./manage.py game_shell` command.
"""

from mock import mock, patch

from django.test import TestCase
from party.models.party import Game, Party, Creature
from party.models.combat import Combat, Combatant
from party.management.commands import game_shell

def fake_roller(number, sides, bonus):
    """
    A fake die roller that always rolls maximum.
    """
    return number * sides + bonus


class ShellTestCase(TestCase):

    @classmethod
    def setUpClass(cls):
        cls._create_patches()
        return super(ShellTestCase, cls).setUpClass()
    
    def setUp(self):
        # only call this once or django raises a warning
        from django.apps.registry import apps
        if 'combatproxy' not in apps.all_models['party'].keys():
            game_shell._create_proxy_models()
        
        # These are ORM objects; don't use these when interacting with shell;
        # instead use self.shell.Combat, etc.
        self.game = Game.objects.create(name='testgame')
        self.party = Party.objects.create(name='testparty', game=self.game)
        self.combat = Combat.objects.create(game=self.game)

        self.fighter = Creature.objects.create(name='fighter', type='PC',
                                               stats={'Initiative': 1, 'Spot': 3},
                                               party=self.party, game=self.game)
        self.rogue = Creature.objects.create(name='rogue', type='PC',
                                             stats={'Initiative': 5, 'Spot': 7},
                                             party=self.party, game=self.game)

        self.goblin = Creature.objects.create(name='goblin', type='monster',
                                              stats={'Initiative': 0, 'Spot': 1},
                                              game=self.game)

        self.npc = Creature.objects.create(name='npc', type='NPC',
                                           stats={'Initiative': -3, 'Spot': 2},
                                           party=self.party, game=self.game)

        self.shell = game_shell.Command()
        self.shell._create_stdout()  # we aren't calling handle(), so need to do this explicitly

        self._start_patches()

        # this resets the 'cached' copy of objects in game_shell
        self._load_objects()

    def tearDown(self):
        self._stop_patches()

    @classmethod
    def _create_patches(cls):
        cls.patches = {}
        cls.patches['print_stdout'] = patch('party.management.commands.game_shell.print_stdout')

    def _start_patches(self):
        for patch_name in self.patches:
            self.patches[patch_name].start()

    def _stop_patches(self):
        for patch_name in self.patches:
            self.patches[patch_name].stop()

    def _get_printed_output(self, mock_print_stdout):
        """
        Get the strings sent to the mocked print_stdout() command.
        """
        return [ca[0][0] for ca in mock_print_stdout.call_args_list]

    def test_load_objects(self):
        """
        Mimics the initial object-loading behavior in the shell, as if you'd typed:
        ./manage.py game_shell
        With no parameters.
        """
        game, party, combat = self._load_objects()
        self.assertEqual(game.pk, self.game.pk)
        self.assertEqual(game_shell.game, self.game)
        self.assertEqual(party, self.party)
        self.assertEqual(game_shell.party, self.party)
        
        self.assertEqual(party.members.count(), 3)  # fighter, rogue, npc
        
        self.assertEqual(combat, self.combat)
        self.assertEqual(game_shell.combat, self.combat)

    def test_load_objects_no_combat(self):
        """
        Running the `game_shell` command without a combat object should create a new one.
        """
        self.combat.delete()

        game, party, combat = self._load_objects()
        self.assertEqual(combat.game, self.game)

    def _load_objects(self):
        """
        Quick and dirty method for invoking the default handle() behavior for loading objects.
        """
        game = self.shell.load_game(None)
        party = self.shell.load_party(None, game=game)
        party.load_members()
        combat = self.shell.load_combat(None, game=game)

        return (game, party, combat)

    def test_load_other_game(self):
        """
        Should be able to load different games by ID
        """
        new_game = Game.objects.create(name='new_game')
        loaded_game = self.shell.load_game(new_game.pk)
        self.assertEqual(new_game, loaded_game)
        self.assertEqual(game_shell.game, loaded_game)

        loaded_game = self.shell.load_game(new_game.name)
        self.assertEqual(new_game, loaded_game)
        self.assertEqual(game_shell.game, loaded_game)

    def test_load_b_team(self):
        """
        Can load a different party belonging to same game, by name or id
        """
        b_team = Party.objects.create(name='b team', game=self.game)

        # by PK
        loaded_party = self.shell.load_party(b_team.pk, game=self.game)
        self.assertEqual(loaded_party, b_team)
        self.assertEqual(game_shell.party, loaded_party)

        # or by name
        loaded_party = self.shell.load_party(b_team.name, game=self.game)
        self.assertEqual(loaded_party, b_team)
        self.assertEqual(game_shell.party, loaded_party)
        
    def test_load_alternate_combat(self):
        """
        In case you've prepped another combat for some reason, you can load it in the shell by ID
        """
        other_combat = Combat.objects.create(game=self.game)

        loaded_combat = self.shell.load_combat(other_combat.pk, game=self.game)
        self.assertEqual(loaded_combat, other_combat)
        self.assertEqual(game_shell.combat, other_combat)

    def test_print_stdout(self):
        self._stop_patches()
        with mock.patch('party.management.commands.game_shell.stdout') as mock_stdout:
            game_shell.print_stdout('test output')
        self.assertEqual(mock_stdout.write.call_args[0][0], 'test output')
        self._start_patches()  # silly, restart them so teardown doesn't fail

    def test_help(self):
        """
        Calling help() should print a bunch of stuff.
        """
        with patch('party.management.commands.game_shell.print_stdout') as mock_print_stdout:
            game_shell.help()

        mock_print_stdout.assert_called()

    def test_creature_roll(self):
        creature = game_shell.Creature(name='test_creature',
                                       type='PC',
                                       stats={'Spot': 5})
        self.assertTrue(creature.roll('Spot') > 5)

        with patch('party.management.commands.game_shell.raw_input_no_history') as raw_input:
            raw_input.return_value = 6
            self.assertTrue(creature.roll('Some Stat') > 6)
            raw_input.assert_called_once()

        self.assertEqual(creature.stats['Some Stat'], 6)

    def test_creature_roll_skip(self):
        """
        When you don't answer a prompt for a stat, it skips it.
        """
        creature = game_shell.Creature(name='test_creature',
                                       type='PC'
                                       )
        with patch('party.management.commands.game_shell.raw_input_no_history') as raw_input:
            raw_input.return_value = ""
            self.assertIsNone(creature.roll("Missing Stat"))
        self.assertTrue('Missing Stat' not in creature.stats)

    def test_creature_stat_block(self):
        creature = game_shell.Creature(name='test_creature',
                               type='PC',
                               stats={'Spot': 4,
                                      'Listen': 3}
                               )
        self.assertEqual(creature.__repr__(),
                         'test_creature\n-------------\nListen: 3\nSpot: 4')

    def test_party_roll(self):
        Creature.objects.all().delete()  # start with fresh PCs
        game_shell.party.pcs = game_shell.CharacterPool()
        
        one = Creature.objects.create(game=game_shell.game, party=game_shell.party,
                                      name='one', type='PC', stats={'Spot': 25})
        two = Creature.objects.create(game=game_shell.game, party=game_shell.party,
                                      name='two', type='PC', stats={'Spot': 0})

        # add characters to party; this API sucks, will probably change
        game_shell.party.add_character('one', type='PC')
        game_shell.party.add_character('two', type='PC')

        with patch('party.management.commands.game_shell.print_stdout') as mock_print_stdout, \
            patch('party.models.party.roll_dice', fake_roller):

            game_shell.party.roll('Spot')

        outputs = self._get_printed_output(mock_print_stdout)
        self.assertTrue('one:\t45' in outputs)
        self.assertTrue('two:\t20' in outputs)

    def test_roll_natural_20(self):
        """
        When a character rolls a natural 20, it should be reported to stdout.
        """
        with patch('party.management.commands.game_shell.print_stdout') as mock_print_stdout, \
            patch('party.models.party.roll_dice', fake_roller):

            game_shell.party.pcs.fighter.roll('Spot')
        
        outputs = self._get_printed_output(mock_print_stdout)
        self.assertTrue('* fighter rolled a natural 20 for Spot' in outputs)

    def test_start_combat_missing_initiative(self):
        """
        The game shell should prompt for the initiative stat if a combatant is missing it.
        """
        self.goblin.stats = {}
        self.goblin.save()
        game_shell.combat.add(game_shell.party)
        game_shell.combat.add(self.goblin)

        with patch('party.models.party.roll_dice', fake_roller), \
            patch('party.management.commands.game_shell.raw_input_no_history') as rinh:

            rinh.return_value = '0'  # reply with '0'
            game_shell.combat.start()

        self.assertEqual(rinh.call_count, 1)
        self.assertEqual(game_shell.combat.get_combatant_by_name('goblin').initiative, 20)

    def test_stop_combat(self):
        """
        `>> combat.stop()` ends the combat but keeps all combatants; removes their initiative rolls and turn
        order.  Useful if something happens to stop and restart a fight.
        """
        game_shell.combat.add(game_shell.party)
        game_shell.combat.add(self.goblin)
        game_shell.combat.start()
        game_shell.combat.current_round = 5
        game_shell.combat.save()
        self.assertTrue(game_shell.combat.started)
        self.assertEqual(game_shell.combat.combatants.count(), 4)

        game_shell.combat.stop()
        
        self.assertEqual(game_shell.combat.combatants.count(), 4)

        self.assertFalse(game_shell.combat.started)

        self.assertEqual(game_shell.combat.positions, [])
        self.assertIsNone(game_shell.combat.current_turn)
        self.assertEqual(game_shell.combat.current_round, 5)

    def test_clear_combat(self):
        """
        `>> combat.clear()` ends the combat and removes all combatants not in the party
        """
        game_shell.combat.add(game_shell.party)
        game_shell.combat.add(self.goblin)
        game_shell.combat.start()
        game_shell.combat.current_round = 5
        game_shell.combat.save()

        self.assertTrue(game_shell.combat.started)
        self.assertEqual(game_shell.combat.combatants.count(), 4)

        game_shell.combat.clear()

        self.assertEqual(game_shell.combat.combatants.count(), 3)
        for combatant in game_shell.combat.combatants.all():
            self.assertTrue(combatant.creature in game_shell.party.pcs)
        self.assertFalse(game_shell.combat.started)

    def test_clear_all_combat(self):
        """
        Combat.clear_all() should remove everybody.
        """
        game_shell.combat.add(game_shell.party)
        game_shell.combat.add(self.goblin)
        game_shell.combat.start()
        game_shell.combat.current_round = 5
        game_shell.combat.save()

        self.assertTrue(game_shell.combat.started)
        self.assertEqual(game_shell.combat.combatants.count(), 4)

        game_shell.combat.clear_all()

        self.assertEqual(game_shell.combat.combatants.count(), 0)
        self.assertFalse(game_shell.combat.started)
        self.assertEqual(game_shell.combat.current_round, 0)

    def test_combat_add_party(self):
        """
        Combat.add() should handle Party objects with or without the 'pcs' collection.
        """
        party = Party.objects.get(pk=game_shell.party.pk)
        self.assertFalse(hasattr(party, 'pcs'))

        game_shell.combat.add(party)
        self.assertEqual(game_shell.combat.combatants.count(), 3)

    def test_combat_add_late_combatant_monster(self):
        """
        When adding a combatant after it has already started, shell should prompt for whether to
        move high-init monsters to the current init count.
        """
        game_shell.combat.add(game_shell.party)
        with patch('party.models.party.roll_dice', fake_roller):  # fix the turn order
            game_shell.combat.start()  # rogue

        game_shell.combat.advance_turn()  # fighter
        npc_combatant = game_shell.combat.advance_turn()  # npc at 17

        with patch('party.models.party.roll_dice', fake_roller), \
            patch('party.management.commands.game_shell.raw_input_no_history') as rinh:
            rinh.return_value = 'Y'
            game_shell.combat.add_monster('goblin')

        self.assertEqual(game_shell.combat.current_turn.creature, self.goblin)

        goblin_combatant = game_shell.combat.current_turn
        self.assertEqual(goblin_combatant.initiative, npc_combatant.initiative)

    def test_combat_add_late_combatant_pc(self):
        """
        When adding a combatant after it has already started, shell should prompt for whether to
        move high-init monsters to the current init count.
        """
        game_shell.combat.add(self.fighter)
        game_shell.combat.add(self.npc)
        game_shell.combat.add(self.goblin)
        with patch('party.models.party.roll_dice', fake_roller):  # fix the turn order
            game_shell.combat.start()  # rogue

        while game_shell.combat.current_turn.initiative >= 20:
            game_shell.combat.advance_turn()

        npc_combatant = game_shell.combat.current_turn

        with patch('party.models.party.roll_dice', fake_roller), \
            patch('party.management.commands.game_shell.raw_input_no_history') as rinh:
            rinh.return_value = 'Y'
            game_shell.combat.add(self.rogue)

        self.assertEqual(game_shell.combat.current_turn.creature, self.rogue)

        rogue_combatant = game_shell.combat.current_turn
        self.assertEqual(rogue_combatant.initiative, npc_combatant.initiative)

    def test_combat_add_late_combatant_no_advance_to_now(self):
        """
        DM has the option to just add a monster and not make it take the next turn on a high init roll.
        """
        game_shell.combat.add(game_shell.party)
        with patch('party.models.party.roll_dice', fake_roller):  # fix the turn order
            game_shell.combat.start()  # rogue

        game_shell.combat.advance_turn()  # fighter
        npc_combatant = game_shell.combat.advance_turn()  # npc at 17

        with patch('party.models.party.roll_dice', fake_roller), \
            patch('party.management.commands.game_shell.raw_input_no_history') as rinh:
            rinh.return_value = 'N'
            game_shell.combat.add_monster('goblin')

        # Goblin gets a 20 init, but we didn't make it the goblin's turn, so nothing changes
        self.assertEqual(game_shell.combat.current_turn, npc_combatant)

        creatures = [c.creature for c in game_shell.combat.combatants_by_position]
        self.assertEqual(creatures,
                         [game_shell.party.pcs.rogue,
                          game_shell.party.pcs.fighter,
                          self.goblin,
                          game_shell.party.pcs.npc])
    
    def test_ready_context_aware(self):
        """
        ready(reason) operates on current combatant.
        """
        game_shell.combat.add(game_shell.party)
        game_shell.combat.add(self.goblin)
        with patch('party.models.party.roll_dice', fake_roller):  # fix the turn order
            game_shell.combat.start()
        game_shell.combat.current_round = 3
        game_shell.combat.save()

        ready_reason = "Waiting to backstab goblin."
        new_turn = game_shell.ready(ready_reason)

        self.assertEqual(new_turn.creature, self.fighter)

        rogue_combatant = game_shell.combat.combatants.filter(creature=self.rogue).get()
        self.assertEqual(rogue_combatant.ready_reason, ready_reason)
        self.assertEqual(rogue_combatant.is_ready, True)

    def test_ready_no_combat(self):
        """
        Trying to ready an action outside combat is an error.
        """
        with self.assertRaises(RuntimeError):
            game_shell.ready("No reason")

    def test_ready_trigger(self):
        """
        Test for triggering a readied action, moving readied character to current turn.
        """
        game_shell.combat.add(game_shell.party)
        game_shell.combat.add(self.goblin)
        with patch('party.models.party.roll_dice', fake_roller):  # fix the turn order
            game_shell.combat.start()
        
        rogue_combatant = game_shell.combat.current_turn
        rogue_combatant.is_ready = True
        rogue_combatant.ready_reason = "Backstabbin'"
        rogue_combatant.save()

        game_shell.combat.advance_turn()  # fighter
        goblin_combatant = game_shell.combat.advance_turn()  # goblin

        game_shell.ready_trigger('rogue')
        rogue_combatant.refresh_from_db()
        self.assertEqual(game_shell.combat.current_turn, rogue_combatant)
        self.assertEqual(rogue_combatant.initiative, goblin_combatant.initiative)
        self.assertEqual(rogue_combatant.is_ready, False)
        self.assertEqual(rogue_combatant.ready_reason, None)

    def test_ready_trigger_creature(self):
        """
        Can use a creature object (e.g. party.pcs.Bob) to select who is taking the triggered action.
        """
        game_shell.combat.add(game_shell.party)
        game_shell.combat.add(self.goblin)
        with patch('party.models.party.roll_dice', fake_roller):  # fix the turn order
            game_shell.combat.start()
        
        rogue_combatant = game_shell.combat.current_turn
        game_shell.ready(reason="Backstabbin'")  # should be rogue
        game_shell.combat.advance_turn()  # fighter
        goblin_combatant = game_shell.combat.advance_turn()  # goblin

        game_shell.ready_trigger(game_shell.party.pcs.rogue)

        rogue_combatant.refresh_from_db()
        
        self.assertEqual(game_shell.combat.current_turn, rogue_combatant)
        self.assertEqual(rogue_combatant.initiative, goblin_combatant.initiative)
        self.assertEqual(rogue_combatant.is_ready, False)
        self.assertEqual(rogue_combatant.ready_reason, None)

    def test_ready_trigger_not_readied(self):
        """
        Regardless of how we select the combatant, if it is not readied, don't move their turn.
        """
        game_shell.combat.add(game_shell.party)
        game_shell.combat.add(self.goblin)
        with patch('party.models.party.roll_dice', fake_roller):  # fix the turn order
            game_shell.combat.start()

        rogue_combatant = game_shell.combat.current_turn
        original_rogue_initiative = rogue_combatant.initiative
        self.assertEqual(rogue_combatant.is_ready, False)
        
        game_shell.combat.advance_turn()  # fighter
        goblin_combatant = game_shell.combat.advance_turn()  # goblin

        with self.assertRaises(Combatant.DoesNotExist):
            game_shell.ready_trigger('rogue')
        rogue_combatant.refresh_from_db()
        self.assertEqual(rogue_combatant.initiative, original_rogue_initiative)

        self.assertEqual(game_shell.ready_trigger(rogue_combatant), False)
        rogue_combatant.refresh_from_db()
        self.assertEqual(rogue_combatant.initiative, original_rogue_initiative)

        with self.assertRaises(Combatant.DoesNotExist):
            game_shell.ready_trigger(game_shell.party.pcs.rogue)
        rogue_combatant.refresh_from_db()
        self.assertEqual(rogue_combatant.initiative, original_rogue_initiative)

    def test_kill(self):
        """
        kill() marks the current turn dead.
        """
        game_shell.combat.add(self.goblin)
        game_shell.combat.start()

        game_shell.kill()
        
        self.assertEqual(game_shell.combat.current_turn.is_dead, True)

    def test_kill_other(self):
        """
        kill() takes args and can mark somebody else dead other than current combatant.
        """
        game_shell.combat.add(game_shell.party)
        game_shell.combat.add(self.goblin)
        with patch('party.models.party.roll_dice', fake_roller):  # fix the turn order
            game_shell.combat.start()

        # goblin is not dead
        self.assertEqual(game_shell.combat.get_combatant_by_name('goblin').is_dead,
                         False)

        game_shell.kill('goblin')

        # goblin is dead
        self.assertEqual(game_shell.combat.get_combatant_by_name('goblin').is_dead,
                         True)

    def test_kill_no_combat(self):
        """
        Running kill() when no combat started is an error.
        """
        with self.assertRaises(RuntimeError):
            game_shell.kill()

    def test_unhide(self):
        goblin_combatant = game_shell.combat.add(self.goblin)
        goblin_combatant.mark_hidden()
        
        game_shell.combat.start()
        game_shell.unhide()
        goblin_combatant.refresh_from_db()
        self.assertEqual(goblin_combatant.is_hidden, False)

    def test_unhide_no_combat(self):
        """
        Running unhide() when no combat started is an error.
        """
        with self.assertRaises(RuntimeError):
            game_shell.unhide()
