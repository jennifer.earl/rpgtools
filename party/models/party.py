from django.db import models
from rpgtools.compat import JSONField
from collections import OrderedDict
from party.utils.mechanics import roll_dice, digest_stat_name
from party.models.base import TimeStampModel
from uuid import uuid4

class Edition(models.Model):
    """
    Represents an edition of rules; used mainly to associate rule objects with specific
    versions of D&D; also stores a list of default stats for new monsters / characters.
    """
    name = models.CharField(max_length=32)
    is_official = models.BooleanField(default=True)
    default_stats = JSONField(default=dict)


class Game(models.Model):
    """
    Generic collection object for objects belonging to a specific campaign.
    """
    name = models.CharField(max_length=256, unique=True)
    description = models.TextField()
    secret_key = models.CharField(max_length=256, unique=True, null=True,
                                  help_text='Password for DM-only views')
    # TODO:  Implement "copy from" command, to duplicate monsters from one game to another

    class Meta:
        get_latest_by = 'pk'

    def set_secret_key(self, new_key=None):
        if new_key is None:
            new_key = str(uuid4()).replace('-', '')
        self.secret_key = new_key
        self.save(update_fields=('secret_key',))


class Party(TimeStampModel):
    """
    A model to attach party-level objects to.  Such as:
    - PCs
    - NPC followers
    - Party treasure
    - DM notes
    """
    game = models.ForeignKey(Game, null=True)
    name = models.CharField(max_length=256, unique=True)
    # TODO:  Owner

    class Meta:
        get_latest_by = 'updated_at'

    @classmethod
    def load(cls, name=None):
        """
        Helper method to load a party; either last-used or by name.
        TODO: permissions, ownership
        """
        if name:
            party = cls.objects.get(name=name)
        else:
            party = cls.objects.latest()
        party.save()  # update updated_at for next time
        return party

    @property
    def saved_characters(self):
        return self.members.filter(type__in=('PC', 'NPC')) 


class Creature(models.Model):
    """
    A generic type storing information about any potential participant in a combat; PC, NPC, monster, etc.

    This is a sparse table; not all fields pertain to all participant types.
    
    The interface should pave over the complexities for using the same object for multiple types;
    this is easier than dealing with multi-table inheritance or abstract base classes with
    a through table between combat and participant...

    "previous_stats" stores a copy of stats whenever the creature is levelled up
    """
    CREATURE_TYPES = [('PC', 'PC'),
                      ('NPC', 'NPC'),
                      ('monster', 'monster')]
    
    name = models.CharField(max_length=256, blank=False, null=False)
    type = models.CharField(choices=CREATURE_TYPES, max_length=len('monster'))
    game = models.ForeignKey(Game, null=True,
        help_text="Game this creature belongs to; None if universal, but PCs should never be universal.")
    party = models.ForeignKey(Party, null=True, related_name='members',
        help_text='Party this creature belongs to, if PC or NPC')
    
    stats = JSONField(default=OrderedDict,
                      help_text="Mapping of skill/ability name to score")
    base_effects = JSONField(default=list,
                             help_text="List of effects creature starts most fights with.")
    max_hit_points = models.IntegerField(null=True, blank=True)    

    level = models.IntegerField(default=1)
    previous_stats = JSONField(
        default=OrderedDict,
        help_text="Backup of stats at previous level, for lazy failover"
    )

    class Meta:
        # For any given game, you can only have one 'goblin' and one 'Templeton' and one
        # 'Sagey McSageface'
        unique_together = ('name', 'type', 'game')

    def __str__(self):
        return "{PK}: {NAME}".format(PK=self.pk, NAME=self.name)

    def level_up(self, level=None):
        """
        Handles changing a character's level:
        - sets new level
        - updates backup stats with current stats
        - empties current stats (for future prompting)
        """
        if level is None:
            level = self.level + 1
        self.level = level
        self.previous_stats.update(self.stats)
        self.stats = {}
        self.save(update_fields=('level', 'stats', 'previous_stats'))

    def get_stat(self, stat, use_previous=True):
        # Start with exact match
        if stat in self.stats:
            return self.stats[stat]

        # Try matching digested version
        stats_digested = {digest_stat_name(k): v for k,v in self.stats.items()}
        stat_digested = digest_stat_name(stat)
        if stat_digested in stats_digested:
            return stats_digested[stat_digested]

        # Try getting it out of the previous level's stats
        if use_previous:
            if stat in self.previous_stats:
                return self.previous_stats.get(stat)
            previous_digested = {digest_stat_name(k): v for k,v in self.previous_stats.items()}
            if stat_digested in previous_digested:
                return previous_digested[stat_digested]

        return None
    
    def set_stat(self, stat, amount):
        if not self.pk:
            self.save()  # can't do partial saves without a PK
        update_fields = ['stats']
        if stat in self.stats:
            self.previous_stats[stat] = amount
            update_fields.append('previous_stats')

        self.stats[stat] = int(amount)
        self.save(update_fields=update_fields)

    def roll(self, stat_name):
        stat = self.get_stat(stat_name)
        if stat is None:
            return None  # deal with prompting later
        
        # TODO: Support mechanics other than d20
        return roll_dice(1, 20, stat)

    def roll_initiative(self):
        return self.roll('Initiative')  # TODO: support other init systems
