from .base import TimeStampModel
from django.db import models
from party.models.party import Game
from party.utils.mechanics import digest_stat_name


class CanonicalStat(TimeStampModel):
    canonical_stat_name = models.CharField(max_length=256, null=False, blank=False)
    canonical_stat_name_digested = models.CharField(max_length=256, null=False, blank=True)
    game = models.ForeignKey(Game, null=True, blank=True,
                             help_text="Game this stat belongs to; none for universal")

    def save(self, *args, **kwargs):
        """
        Override save to specify digested form of stat name.
        """
        self.canonical_stat_name_digested = digest_stat_name(self.canonical_stat_name)
        return super().save(*args, **kwargs)


class StatAlias(TimeStampModel):
    """
    Maps a stat alias to a canonical name.  Useful for resolving typos, fixing punctuation, etc.
    """
    canonical_stat = models.ForeignKey(CanonicalStat, null=False, blank=False)
    alias = models.CharField(max_length=256, null=False, blank=False,
                             help_text='String alias that can be used in place of canonical string.')
    alias_digested = models.CharField(max_length=256, null=False, blank=True)

    @staticmethod
    def get_canonical_stat(alias_string, game=None):
        """
        Given a string, try to find the CanonicalStat for it.
        """
        alias_string_digested = digest_stat_name(alias_string)
        # First: case-insensitive exact match in CanonicalStat
        canonical_game_stats = CanonicalStat.objects.filter(game=game)
        try:
            return canonical_game_stats.get(canonical_stat_name__iexact=alias_string)
        except CanonicalStat.DoesNotExist:
            pass

        # Second: try digested form of canonical stat name
        try:
            return canonical_game_stats.get(canonical_stat_name_digested=alias_string_digested)
        except CanonicalStat.DoesNotExist:
            pass

        # Third: case-insensitive exact match in StatAlias
        game_stat_aliases = StatAlias.objects.filter(canonical_stat__game=game)
        try:
            return game_stat_aliases.get(alias__iexact=alias_string).canonical_stat
        except StatAlias.DoesNotExist:
            pass

        # Fourth: digested stat alias
        try:
            return game_stat_aliases.get(alias_digested=alias_string_digested).canonical_stat
        except StatAlias.DoesNotExist:
            pass

        return None

    def save(self, *args, **kwargs):
        """
        Override save to specify digested form of stat name.
        """
        self.alias_digested = digest_stat_name(self.alias)
        return super().save(*args, **kwargs)
