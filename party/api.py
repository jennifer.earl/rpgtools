from tastypie import fields
from tastypie.resources import ModelResource
from party.models.combat import Combat, Combatant

class CombatantResource(ModelResource):
    class Meta:
        queryset = Combatant.objects.all()

class CombatResource(ModelResource):
    combatants = fields.ToManyField(CombatantResource,
                                    attribute=lambda bundle: get_combatants_by_position(bundle),
                                    null=True, full=True)
    current_turn = fields.ToOneField(CombatantResource,
                                     attribute=lambda bundle: get_current_turn(bundle),
                                     related_name='current_turn',
                                     null=True, full=True)

    class Meta:
        queryset = Combat.objects.all()
        resource_name = 'combats'
        list_allowed_methods = ['get',]
        detail_allowed_methods = ['get',]  # TODO: Delete with auth

def get_combatants_by_position(bundle):
    """
    Filters out hidden combatants if the game's secret key is not provided.
    """
    combat = bundle.obj
    request = bundle.request
    if not combat:
        return None
    if combat.game.secret_key and combat.game.secret_key == request.GET.get('sk'):
        return combat.all_combatants_by_position
    else:
        return combat.combatants_by_position

def get_current_turn(bundle):
    """
    Deals with hiding current turn's combatant if the secret_key isn't provided and the current
    combatant is hidden.
    """
    combat = bundle.obj
    request = bundle.request
    if not combat:
        return None
    
    dm_view = bool(combat.game.secret_key and combat.game.secret_key == request.GET.get('sk'))
    current_combatant = combat.current_turn

    if not current_combatant.is_hidden or dm_view:
        return current_combatant

    # otherwise, we'll need to find the previous non-hidden combatant
    unhidden_combatants = combat.combatants_by_position
    if not unhidden_combatants:
        return None  # everybody's hidden, nothing to do

    all_combatants = combat.all_combatants_by_position
    position_idx = combat.positions.index(current_combatant.pk)

    # rearrange the combatants so that the current one is at the end of the reversed list
    ordered_combatants = all_combatants[position_idx:] + all_combatants[:position_idx]
    ordered_combatants.reverse()
    for combatant in ordered_combatants:
        if not combatant.is_hidden:
            return combatant  # found the first one before the current one that isn't hidden
        elif combatant == current_combatant:
            # we made it all the way around the list, which shouldn't be possible
            return None
    